window.$ = window.jQuery = $;
require('../scss/app.scss');
// require from vendors start
require("./vendors/fencybox.js");
require("./vendors/slick-slider");
require("./vendors/toast");
// require from vendors end


require("./simply-grid.js");
require("./welcome-popup.js");
require("./add-cart.js")
require('./cart-quntity');
// if Header sticky require use sticky-header.js file
// require('./sticky-header')
