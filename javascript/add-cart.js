// add product  to cart object
import { addToCartButton } from "./helper";
class AddNewProductToCart {
  constructor() {
    this.init();
  }
  init = () => {
    $('.cancle').on('click',function(){
      $(".cart-side-bar").removeClass('active');
    });
    $(".add-to-cart-button").on("click", function (e) {
      console.log('new1')
       //take variant id from form
       let button=$(this)
       const  productVariantId = $(this).closest(".myform").find("input[name='variant_id']").val();
       const productData={
        id:productVariantId,
        quantity:1
      }
       button.val('adding')
     /*
         add to cart function import from helper file to add product in cart
         @param {number} product variant id
      */
    
     const afterclick=()=>{
      $(".cart-side-bar").addClass('active');
      button.val('order now')
     }
      addToCartButton(productData,afterclick)
    });

  };
}
new AddNewProductToCart;
