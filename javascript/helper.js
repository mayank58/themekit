/* 
  create cookies function 
  @param {string} cname is cookie name
  @param {string} cvalue is cookies value
  @param {number}  exdat is expiry date of cookies
 */
export const setCookie = (cname, cvalue, exdays) => {
  console.log(exdays, "expire data");
  var d = new Date();
  d.setTime(d.getTime() + exdays * 1000 * 60 * 60 * 24);
  var expires = "; expires=" + d.toUTCString();
  console.log(expires);
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};
/* 
   get cookie data function
   @param {string} cname is cookie name
*/
export const getCookie = (cname) => {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};
/* 
  add to cart function for post request
  @param {number} product variant id
  @param {callback} call back function when data load in sidecart div then side cart display
*/
export const addToCartButton =  (productVData,callback) => {
  const {id,quantity}=productVData
  fetch('/cart/add.js', {
    method: 'POST',
    
   headers: {
     "Content-Type": "application/json",
     "Accept": "application/json",
   },
    body: JSON.stringify({
      id: parseInt(id),
      quantity: quantity,
    })
  })
  .then(response => {
    return response.text();
  })
  .then(result => {
    $.ajax({
      url:	"/cart?view=side-cart",
      type:	'GET'
    })
    .done(function(data) {
      document.querySelector(".cart-input-quantity").innerHTML = data;
      callback()

    })
  

  })
  .catch((error) => {
    console.error('Error:', error);
  });
};


/*
    chnage cart data for post request
    @param(number , number)    cart  key =variant id (number) and  qty= quantitty of product
    
*/
export const changeCartData=(key,qty)=>{
  var cartObject = {
    'id': key,
    'quantity': qty
  }
  $.ajax({
    url: "cart/change.js",
    type: "POST",
    data: cartObject,
    dataType:'json',
    encode: true,
  }).done (function(data){
    $.ajax({
      url:	"/cart?view=side-cart",
      type:	'GET'
      })
      .done(function(data) {
      document.querySelector(".cart-input-quantity").innerHTML = data;
    
      })
      .fail(function(data) {})
      .always(function(data) {});
    

  }).fail(function(data){
    $.toast({
      heading: data.responseJSON.message,
      text: data.responseJSON.description,
      showHideTransition: 'slide',
      icon: 'error'
    })
  
  });
}