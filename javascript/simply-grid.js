class Simplygrid {
  constructor() {
    this.init();
  }
  init = () => {
    $(".slider-for").slick({
      slidesToShow: 3,
      slidesToScroll: 3,
      prevArrow: $(".prev"),
      nextArrow: $(".next"),
    });
  };
}

new Simplygrid();
