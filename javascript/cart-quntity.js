
/*
  cart qulity increament and decreamnet function
*/

import { changeCartData } from "./helper";

class Cartqunatity{
    constructor(){
        this.init();
    }
     init=()=>{

			$(document).on( "click","#minus",function () {
                const key = $(this).attr("data-key");
				const $input = $(this).parent().find('.quality');
				$input.val(parseInt($input.val()) - 1);
				const qty = parseInt($(this).attr("data-quantity"));
				// cart change function for quantity chnage import from helper file
				changeCartData(key,qty-1);
			});
			$(document).on( "click",".plus",function () {
                const key = $(this).attr("data-key");
				const $input = $(this).parent().find('.quality');
				$input.val(parseInt($input.val()) +1);
				const qty = parseInt($(this).attr("data-quantity"));
				// cart change function for quantity chnage import from helper file
			    changeCartData(key,qty+1);

			});
			$(document).on( "click",".remove",function (e) {
				console.log('clik')
				e.preventDefault()
				const key = $(this).attr("data-key");
				changeCartData(key,0);

				
			})

    }
}
new Cartqunatity;