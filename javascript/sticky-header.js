class StickyHeader{
    constructor (){
        this.init()
    }
    init=()=>{
        var offset = $( ".top-navigation-wrapper" ).offset();
        var sticky = document.getElementById(".top-navigation-wrapper")
        
        $(window).scroll(function() {
        
            if ( $(window).scrollTop() > offset.top){
                $('.top-navigation-wrapper').addClass('sticky');
            } else {
                 $('.top-navigation-wrapper').removeClass('sticky');
            } 
        
        });
}
}
new StickyHeader;